const promise = fetch('api/top10EconomicalBowlers2015' , {
    method: 'GET'
})
promise.then((res) => {
    return res.json();
}).then(value => {
    bowler = []
    economy = []
    value.forEach((ele) => {
        bowler.push(ele['bowlerName']);
        economy.push(ele['bowlerEconomy']);
    })
    // economy = economy.map((ele) => {
    //     return ele.toFixed(2);
    // })
    // console.log(economy);
    Highcharts.chart('container', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Top 10 Bowlers by Economy'
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle.com</a>'
        },
        xAxis: {
            categories: bowler,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Economy',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Economy',
            data: economy
        }]
    });

})
