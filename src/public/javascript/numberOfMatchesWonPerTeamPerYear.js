const promise = fetch('api/numberOfMatchesWonPerTeamPerYear' , {
    method: 'GET'
})
promise.then((res) => {
    return res.json();
}).then(value => {
    let seasonArr = []
    let dataArr = []
    for (let season in value) {
        if (!seasonArr.includes(season)) {
            seasonArr.push(season);
            dataArr.push(value[season]);
        }
    }
    console.log(seasonArr)
    let result = {}
    dataArr.forEach((data) => {
        for(let team in data) {
            if (!(team in result)) {
                result[team] = []
            }
        }
    })
    dataArr.forEach((data , index) => {
        for (let team in data) {
            result[team].push(data[team]);
        }
        for (let team in result) {
            if (result[team].length < index+1) {
                result[team].push(0);
            }
        }
    })
    result = Object.entries(result);
    result = result.map((team) => {
        return {
            name: team[0],
            data: team[1]
        }
    })
    console.log(result);
    Highcharts.chart('container', {

        title: {
            text: 'Top 10 Bowlers by Economy'
        },

        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle.com</a>'
        },

        yAxis: {
            title: {
                text: 'Wins per team per year'
            }
        },

        xAxis: {
            accessibility: {
                rangeDescription: 'Range: 2008 to 2017'
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2008
            }
        },

        series: result,

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
})
