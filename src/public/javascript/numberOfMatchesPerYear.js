const promise = fetch('api/numberOfMatchesPerYear' , {
    method: 'GET'
})
promise.then((res) => {
    return res.json();
}).then(value => {
    seasons = []
    matches = []
    Object.entries(value).forEach((seasonData) => {
        seasons.push(seasonData[0]);
        matches.push(seasonData[1]);
    })

    Highcharts.chart('container', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Historic World Population by Region'
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle.com</a>'
        },
        xAxis: {
            categories: seasons,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Population (millions)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' millions'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Number of matches per year',
            data: matches
        }]
    });

})
