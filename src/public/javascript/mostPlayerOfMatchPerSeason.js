const promise = fetch('api/mostPlayerOfMatchPerSeason' , {
    method: 'GET'
})
promise.then((res) => {
    return res.json();
}).then(value => {
    console.log(value);
    dataArr = value.map((seasonObj) => {
        return Object.entries(seasonObj);
    }).sort((a , b) => {
        return a[0][0]-b[0][0];
    })
    console.log(dataArr);
    dataArr = dataArr.map((data) => {
        let finalData = []
        console.log(data[0][1]);
        finalData.push(data[0][0] + " " + data[0][1]['name']);
        finalData.push(data[0][1]['data']);
        return finalData;
    })
    console.log(dataArr);
    // Create the chart
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Most number of Player of the Match Award in respective Years'
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle.com</a>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of player of the Match'
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Number of player of the Match',
            data: dataArr,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
})
