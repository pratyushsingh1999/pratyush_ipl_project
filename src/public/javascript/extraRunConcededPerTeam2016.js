const promise = fetch('api/extraRunConcededPerTeam2016' , {
    method: 'GET'
})
promise.then((res) => {
    return res.json();
}).then(value => {
    teams = []
    extraRuns = []
    Object.entries(value).forEach((teamData) => {
        teams.push(teamData[0]);
        extraRuns.push(teamData[1]);
    })
    result = []
    Highcharts.chart('container', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Extra runs conceded per team in 2016'
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle.com</a>'
        },
        xAxis: {
            categories: teams,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Extra Runs',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Extra runs conceded per team in 2016',
            data: extraRuns
        }]
    });

})
