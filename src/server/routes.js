const iplFunctions = require('./ipl');
const fs = require('fs');
const requestHandler = (req,res) => {
    const url = req.url;
    const method = req.method;

    switch (url) {
        case '/' : {
            fs.readFile('./src/public/index.html','utf8', (err ,html) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(html);
                    res.end();
                }
            })
            break;
        }
        case '/index.js' : {
            fs.readFile('./src/public/index.js','utf8', (err ,js) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(js);
                    res.end();
                }
            })
            break;
        }
        case '/styles.css' : {
            fs.readFile('./src/public/styles.css','utf8', (err ,css) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/css"});
                    res.write(css);
                    res.end();
                }
            })
            break;
        }
        case '/numberOfMatchesPerYear' : {
            fs.readFile('./src/public/chartsComponents/numberOfMatchesPerYear.html','utf8', (err ,html) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(html);
                    res.end();
                }
            })
            break;
        }
        case '/numberOfMatchesPerYear.js' : {
            fs.readFile('./src/public/javascript/numberOfMatchesPerYear.js','utf8', (err ,js) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(js);
                    res.end();
                }
            })
            break;
        }
        case '/numberOfMatchesWonPerTeamPerYear' : {
            fs.readFile('./src/public/chartsComponents/numberOfMatchesWonPerTeamPerYear.html','utf8', (err ,html) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(html);
                    res.end();
                }
            })
            break;
        }
        case '/numberOfMatchesWonPerTeamPerYear.js' : {
            fs.readFile('./src/public/javascript/numberOfMatchesWonPerTeamPerYear.js','utf8', (err ,js) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(js);
                    res.end();
                }
            })
            break;
        }
        case '/extraRunConcededPerTeam2016' : {
            fs.readFile('./src/public/chartsComponents/extraRunConcededPerTeam2016.html','utf8', (err ,html) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(html);
                    res.end();
                }
            })
            break;
        }
        case '/extraRunConcededPerTeam2016.js' : {
            fs.readFile('./src/public/javascript/extraRunConcededPerTeam2016.js','utf8', (err ,js) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(js);
                    res.end();
                }
            })
            break;
        }
        case '/top10EconomicalBowlers2015' : {
            fs.readFile('./src/public/chartsComponents/top10EconomicalBowlers2015.html','utf8', (err ,html) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(html);
                    res.end();
                }
            })
            break;
        }
        case '/top10EconomicalBowlers2015.js' : {
            fs.readFile('./src/public/javascript/top10EconomicalBowlers2015.js','utf8', (err ,js) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(js);
                    res.end();
                }
            })
            break;
        }
        case '/mostPlayerOfMatchPerSeason' : {
            fs.readFile('./src/public/chartsComponents/mostPlayerOfMatchPerSeason.html','utf8', (err ,html) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(html);
                    res.end();
                }
            })
            break;
        }
        case '/mostPlayerOfMatchPerSeason.js' : {
            fs.readFile('./src/public/javascript/mostPlayerOfMatchPerSeason.js','utf8', (err ,js) => {
                if (err) {
                    console.log(err);
                }else {
                    res.writeHeader(200, {"Content-Type": "text/html"});
                    res.write(js);
                    res.end();
                }
            })
            break;
        }
        // APIs for data

        case '/api/numberOfMatchesPerYear' : {
            fs.readFile('./src/public/output/numberOfMatchesPerYear.json' , 'utf8' , (err , data) => {
                res.writeHeader(200, {"Content-Type": "json/application"});
                res.write(data);
                res.end();
            })
            break;
        }
        case '/api/numberOfMatchesWonPerTeamPerYear' : {
            fs.readFile('./src/public/output/numberOfMatchesWonPerTeamPerYear.json' , 'utf8' , (err , data) => {
                res.writeHeader(200, {"Content-Type": "json/application"});
                res.write(data);
                res.end();
            })
            break;
        }
        case '/api/extraRunConcededPerTeam2016' : {
            fs.readFile('./src/public/output/extraRunConcededPerTeam2016.json' , 'utf8' , (err , data) => {
                res.writeHeader(200, {"Content-Type": "json/application"});
                res.write(data);
                res.end();
            })
            break;
        }
        case '/api/top10EconomicalBowlers2015' : {
            fs.readFile('./src/public/output/top10EconomicalBowlers2015.json' , 'utf8' , (err , data) => {
                res.writeHeader(200, {"Content-Type": "json/application"});
                res.write(data);
                res.end();
            })
            break;
        }
        case '/api/mostPlayerOfMatchPerSeason' : {
            fs.readFile('./src/public/output/mostPlayerOfMatchPerSeason.json' , 'utf8' , (err , data) => {
                res.writeHeader(200, {"Content-Type": "json/application"});
                res.write(data);
                res.end();
            })
            break;
        }
        default : {
            res.writeHeader(404) , {'content-type': 'text/html'};
            res.write("Page not found");
            res.end();
        }
    }
}

module.exports = requestHandler;
