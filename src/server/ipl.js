const fs = require('fs');

const matchesFilePath = './src/data/matches.json'
const deliveriesFilePath = './src/data/deliveries.json'

const matchesData = JSON.parse(fs.readFileSync(matchesFilePath));
const deliveriesData = JSON.parse(fs.readFileSync(deliveriesFilePath));

const wonTheTossWonTheMatch = () => {
    let matchData = matchesData[0];
    let count = 0;
    matchData = matchData.filter((match)=> match['toss_winner'] === match['winner'])
    count = matchData.length;
    return count;
}

const mostPlayerOfMatchPerSeason = () => {
    const matchData = matchesData[0];
    let result = []
    const seasonsArray = [];
    matchData.forEach((match) => {
        if (!(match['season'] in result)) {
            result[match['season']] = {};
            seasonsArray.push(match['season']);
        }
    })
    seasonsArray.forEach((season) => {
        const tempData = matchData.filter((match) => match['season'] === season);
        let manOftheMatchPerPlayer = {}
        tempData.forEach((match) => {
            if (match['player_of_match'] in manOftheMatchPerPlayer) {
                manOftheMatchPerPlayer[match['player_of_match']] += 1;
            }else {
                manOftheMatchPerPlayer[match['player_of_match']] = 1;
            }
        })
        let maxManOftheMatch = {
            player: '',
            count: 0,
            season: season
        }
        for(let player in manOftheMatchPerPlayer) {
            if(manOftheMatchPerPlayer[player] >maxManOftheMatch['count']) {
                maxManOftheMatch = {
                    player: player,
                    count: manOftheMatchPerPlayer[player],
                    season: season
                }
            }
        }
        result.push(maxManOftheMatch);
    })
    result = result.filter((ele) => {
        if (ele['player'] === undefined) {
            return false
        }else return true
    }).map((player) => {
        let data = {}
        data[player.season] = {
            name: player.player , data: player.count
        }
        return data;
    })
    fs.writeFile('./src/public/output/' + "mostPlayerOfMatchPerSeason.json" , JSON.stringify(result) , (err) => {
        if (err) {
            return console.log(err);
        } else {
            console.log("File was saved");
        }
    })
    return result;
}

const strikeRateOfBatsmentEachSeason = () => {
    const matchData = matchesData[0];
    const deliveryData = deliveriesData[0];
    const matchIdPerSeason = {}
    matchData.forEach((match) => {
        if (match.season in matchIdPerSeason) {
            matchIdPerSeason[match.season].push(match['id']);
        }else {
            matchIdPerSeason[match.season] = [];
            matchIdPerSeason[match.season].push(match['id']);
        }
    })
    const deliveryDataForAllSeason = {}
    for (let season in matchIdPerSeason) {
        let deliveryDataForASeason = deliveryData.filter((delivery) => {
            if (matchIdPerSeason[season].includes(delivery['match_id'])) {
                return true
            }else {
                return false
            }
        })
        deliveryDataForAllSeason[season] = deliveryDataForASeason;
    }
    let playersDataBySeason = {}
    for (let season in deliveryDataForAllSeason) {
        let dataOfOneSeason = {};
        deliveryDataForAllSeason[season].forEach((delivery) => {
            if (delivery['bowler'] in dataOfOneSeason) {
                dataOfOneSeason[delivery['bowler']]['balls'] += 1;
                dataOfOneSeason[delivery['bowler']]['runs'] += parseInt(delivery['batsman_runs']);
            }else {
                dataOfOneSeason[delivery['bowler']] = {
                    balls: 1,
                    runs: parseInt(delivery['batsman_runs'])
                }
            }
        })
        playersDataBySeason[season] = dataOfOneSeason;
    }
    for (let season in playersDataBySeason) {
        for (let batsmen in playersDataBySeason[season]) {
            playersDataBySeason[season][batsmen] = (playersDataBySeason[season][batsmen]['runs'] / playersDataBySeason[season][batsmen]['balls'])*100
        }
    }
    return playersDataBySeason;
}

const bowlerWithBestEconomySuperOver = () => {
    const deliveryData = deliveriesData[0].filter( (delivery) => {
        return delivery['is_super_over'] == 1;
    })
    let result = {}
    deliveryData.forEach((delivery) => {
        const runs = parseInt(delivery['total_runs']);
        if (delivery['bowler'] in result) {
            result[delivery['bowler']] = {
                bowls: result[delivery['bowler']]['bowls'] + 1,
                runs: result[delivery['bowler']]['runs'] + runs
            }
        }else {
            result[delivery['bowler']] = {
                bowls: 1,
                runs: runs
            }
        }
    })
    let economyArray = []
    for (let bowler in result) {
        economy = (result[bowler]['runs'] / result[bowler]['bowls'])*6
        const data = {
            bowler: bowler,
            economy: economy
        }
        economyArray.push(data);
    }
    economyArray.sort((a,b) => {
        return a['economy']-b['economy']
    })
    return economyArray[0];
}

module.exports = {
    wonTheTossWonTheMatch,
    mostPlayerOfMatchPerSeason,
    strikeRateOfBatsmentEachSeason,
    bowlerWithBestEconomySuperOver
}
