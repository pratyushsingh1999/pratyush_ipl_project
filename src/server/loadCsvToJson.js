// Imports
const csvtojson = require('csvtojson');
const fs = require('fs');

const matchCsvPath = './src/data/matches.csv';
const deliveriesCsvPath = './src/data/deliveries.csv';

const matchesData = []
const deliveriesData = []

csvtojson().fromFile(matchCsvPath).then((ele) => {
    matchesData.push(ele);
}).then(() => {
    const matchesJson = JSON.stringify(matchesData);
    fs.writeFile('./src/data/matches.json' , matchesJson , 'utf8' , (error) => {
        console.log(error);
    });
});

csvtojson().fromFile(deliveriesCsvPath).then((ele) => {
    deliveriesData.push(ele);
}).then(() => {
    const deliveriesJson = JSON.stringify(deliveriesData);
    fs.writeFile('./src/data/deliveries.json' , deliveriesJson , 'utf8' , (error) => {
        console.log(error);
    });
});
