const fs = require('fs');

const matchesFilePath = './src/data/matches.json'
const deliveriesFilePath = './src/data/deliveries.json'

const outputBasePath = './src/public/output/'

const numberOfMatchesPerYear = () => {
    const promise = new Promise((resolve , reject)=> {
        fs.readFile(matchesFilePath , (err ,data) => {
            if (err) {
                console.log("Error occured");
                reject(err);
            }
            const matchData = JSON.parse(data)[0];
            const seasonWiseData = matchData.reduce((obj , match) => {
                if (obj[match['season']] == undefined) {
                    obj[match['season']] = 1;
                }else {
                    obj[match['season']]++;
                }
                return obj
            }, {});
            fs.writeFile(outputBasePath + "numberOfMatchesPerYear.json" , JSON.stringify(seasonWiseData) , (err) => {
                if (err) {
                    return console.log(err);
                } else {
                    console.log("File was saved");
                }
            })
            resolve(seasonWiseData);
        });
    });
    return promise;
}

const numberOfMatchesWonPerTeamPerYear = () => {
    const promise = new Promise((resolve, reject) => {
        fs.readFile(matchesFilePath , (err , data) => {
            if (err) {
                console.log("error occured");
                reject(err);
            }
            const matchData = JSON.parse(data)[0];
            const seasonWiseTeamData = matchData.reduce((obj , match) => {
                if (obj[match['season']] == undefined) {
                    obj[match['season']] = {}
                    if (obj[match['season']][match['winner']] == undefined) {
                        obj[match['season']][match['winner']] = 1;
                    }else {
                        obj[match['season']][match['winner']]++;
                    }
                }else {
                    if (obj[match['season']][match['winner']] == undefined) {
                        obj[match['season']][match['winner']] = 1;
                    }else {
                        obj[match['season']][match['winner']]++;
                    }
                }
                return obj
            }, {})
            fs.writeFile(outputBasePath + "numberOfMatchesWonPerTeamPerYear.json" , JSON.stringify(seasonWiseTeamData) , (err) => {
                if (err) {
                    return console.log(err);
                } else {
                    console.log("File was saved");
                }
            })
            resolve(seasonWiseTeamData);
        });
    });
    return promise;
}

const extraRunConcededPerTeam2016 = () => {
    const matchData = new Promise((resolve, reject) => {
        fs.readFile(matchesFilePath ,(err, data) => {
            if (err) {
                console.log("Error occured");
                reject(err);
            }
            const matchData = JSON.parse(data)[0].filter((match)=> match.season === '2016');
            resolve(matchData);
        })
    })
    const deliveryData = new Promise((resolve, reject) => {
        fs.readFile(deliveriesFilePath, (err,data)  => {
            if (err) {
                console.log("Error reading file");
                reject(err);
            }else {
                const deliveryData = JSON.parse(data)[0]
                resolve(deliveryData);
            }
        })
    })

    return Promise.all([matchData,deliveryData]).then(([matchData , deliveryData]) => {
        const matchIdArr = matchData.map((match) => {
            return match.id
        })
        const deleveryDatafor2016 = deliveryData.filter((delivery) => {
            if (matchIdArr.includes(delivery['match_id'])) {
                return true
            }else {
                return false
            }
        })
        const result = deleveryDatafor2016.reduce((acc, currentDelivery) => {
            if (acc[currentDelivery['bowling_team']] == undefined) {
                acc[currentDelivery['bowling_team']] = parseInt(currentDelivery['extra_runs'])
            }else {
                acc[currentDelivery['bowling_team']] += parseInt(currentDelivery['extra_runs'])
            }
            return acc;
        } , {})
        fs.writeFile(outputBasePath + "extraRunConcededPerTeam2016.json" , JSON.stringify(result) , (err) => {
            if (err) {
                return console.log(err);
            } else {
                console.log("File was saved");
            }
        })
        return result;
    })
}

const top10EconomicalBowlers2015 = () => {
    const matchData = new Promise((resolve, reject) => {
        fs.readFile(matchesFilePath ,(err, data) => {
            if (err) {
                console.log("Error Occured");
                reject(err);
            }
            const matchData = JSON.parse(data)[0].filter((match)=> match.season === '2015');
            resolve(matchData);
        })
    })
    const deliveryData = new Promise((resolve, reject) => {
        fs.readFile(deliveriesFilePath, (err,data)  => {
            if (err) {
                console.log("Error reading file");
                reject(err);
            }else {
                const deliveryData = JSON.parse(data)[0]
                resolve(deliveryData);
            }
        })
    })
    return Promise.all([matchData , deliveryData]).then(([matchData , deliveryData]) => {
        const matchIds = matchData.map((match) => {
            return match.id;
        })
        const deliveriesIn2015 = deliveryData.filter((delivery) => {
            if (matchIds.includes(delivery['match_id'])) {
                return true
            }else {
                return false
            }
        })
        let result = deliveriesIn2015.reduce((acc, delivery) => {
            if (acc[delivery['bowler']] == undefined) {
                acc[delivery['bowler']] = {
                    runs: parseInt(delivery['total_runs']),
                    balls: 1
                }
            }else {
                acc[delivery['bowler']]['runs'] += parseInt(delivery['total_runs']);
                acc[delivery['bowler']]['balls']++;
            }
            return acc;
        }, {})
        result = Object.entries(result).map((val) => {
            const bowlerName = val[0];
            const bowlerEconomy = (val[1].runs / val[1].balls)*6;
            return {
                bowlerName,
                bowlerEconomy
            }
        }).sort((a, b) => {
            return a.bowlerEconomy - b.bowlerEconomy
        }).slice(0,10);
        fs.writeFile(outputBasePath + "top10EconomicalBowlers2015.json" , JSON.stringify(result) , (err) => {
            if (err) {
                return console.log(err);
            } else {
                console.log("File was saved");
            }
        })
        return result;
    })
}

module.exports = {
    numberOfMatchesPerYear,
    numberOfMatchesWonPerTeamPerYear,
    extraRunConcededPerTeam2016,
    top10EconomicalBowlers2015
}
