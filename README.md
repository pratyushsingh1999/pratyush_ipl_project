# Read Me
- This project was created using npm.
- To start just clone the repository using git clone
- After that do npm install to install the dependencies
- After that load the json data in your data directory using command ***npm run loadJson***
- After that load the output json files using command *** npm run loadOutput***
- Now just type npm start in the terminal and enjoy your app.

#### Happy coding !!!
